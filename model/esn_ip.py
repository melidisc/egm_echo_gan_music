import theano.tensor as TT
import theano
import numpy as np
import scipy
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import matplotlib.pyplot as plt

srng = RandomStreams()

class ESNIP:
    def __init__(self,in_s, r_s, out_s, output_sparsity=.2):

        self.in_s = in_s
        self.r_s = r_s
        self.out_s = out_s
        #in and outs
        self.u = TT.matrix(name="input")
        self.v = TT.matrix(name="output")
        self.dt = .9#0.6#TT.scalar(name="time resolution")
        self.y = TT.matrix(name="reservoir")
        self.ip_lr = 0.00001#TT.scalar(name="IP learning rate")
        self.miou = 0.4#TT.scalar(name="Miou")
        self.net_lr = TT.scalar(name="Net learning rate")

        def get_weights(a,b,d,s):
            return s*(scipy.sparse.rand(a,b,d).astype(theano.config.floatX)
                ).todense()

        W0 = get_weights(r_s,r_s,.2,1)
        eigvals = np.linalg.eigvals(W0)

        lamda = np.max(eigvals)

        W1 = (1./np.absolute(lamda)) * W0

        self.W_res_res = theano.shared(
        (.8 * W1).astype(theano.config.floatX)
        )

        #from reservoir to
        self.W_inp_res = theano.shared(
            get_weights(in_s,r_s,.1,.5))
            # get_weights(in_s,r_s,.1,1.))

        self.W_out_res = theano.shared(
            get_weights(out_s,r_s,output_sparsity,.5))

        #from input to reservoir
        self.W_res_inp = theano.shared(
            np.zeros((r_s,in_s)).astype(theano.config.floatX)
            )

        #from output to reservoir
        self.W_res_out = theano.shared(
            get_weights(r_s,out_s,output_sparsity,.5))

        self.W_inp_out = theano.shared(
            np.zeros((in_s,out_s)).astype(theano.config.floatX)
            )

        self.ai = theano.shared(
            np.ones(r_s).astype(theano.config.floatX)
            )
        self.bi = theano.shared(
            np.zeros(r_s).astype(theano.config.floatX)
            )

        self.res = theano.shared(
                np.zeros(self.r_s).astype(theano.config.floatX)
            )

        self.out = theano.shared(
                np.ones(self.out_s).astype(theano.config.floatX)
            )


    def set_params(self,):
        self.params = [
            # self.W_inp_res,
            # self.W_out_res,
            # self.W_res_inp,
            self.W_res_out,
            # self.W_inp_out,
            # self.ai,
            # self.bi
        ]

    def IP_change(self, x_k, y_k):
        res_x_k = x_k[self.in_s:-self.out_s]
        res_y_k = y_k[self.in_s:-self.out_s]

        x_ktp1 = self.make_z_k(x_k)
        res_ktp1 = x_ktp1[self.in_s:-self.out_s]

        Db = self.ip_lr*(1.-(2.+(self.miou**(-1.)))*res_y_k \
                        + (self.miou**(-1.))*(res_y_k**2.))

        Da = self.ip_lr*( (self.ai**(-1.)) + res_x_k *Db)

        return Db, Da


    def make_W_net_times_z_k(self, z_k):

        inp = z_k[:self.in_s]
        res = z_k[self.in_s:-self.out_s]
        out = z_k[-self.out_s:]

        #input
        i = TT.dot(inp, self.W_inp_res)
        r = TT.dot(res, self.W_res_res)
        o = TT.dot(out, self.W_out_res)

        #output trainables
        i_r = TT.dot(res, self.W_res_inp)
        o_r = TT.dot(res, self.W_res_out)

        #input to Output
        i_o = TT.dot(inp, self.W_inp_out)

        u = i_r
        y = i + r #+ o
        v = o_r + i_o

        return TT.concatenate([u,y,v])

    def make_x_kp1(self, x_k, z_k):
        c1 = (1.-self.dt)* x_k

        this_shit = self.make_W_net_times_z_k(z_k)

        c2 = self.dt * this_shit

        return c1+c2

    def make_z_k(self, x_k):
        inp = x_k[:self.in_s]
        res = x_k[self.in_s:-self.out_s]
        out = TT.nnet.sigmoid(x_k[-self.out_s:])
        # out = TT.nnet.softmax(x_k[-self.out_s:])[0]

        r = (1.+ TT.exp( -( (self.ai*res) + self.bi ) ) )**-1.

        return TT.concatenate([inp,r,out])

    def scanner(self,u,y,v,ai,bi,r,o):
        x_k = TT.concatenate([u, y, v])
        z_k = self.make_z_k(x_k)
        x_ktp1 = self.make_x_kp1(x_k, z_k)
        z_ktp1 = self.make_z_k(x_ktp1)
        out = z_ktp1[-self.out_s:]
        out += 1e-4*np.random.random(self.out_s)
        inp = z_ktp1[:self.in_s]
        res = x_ktp1[self.in_s:-self.out_s]

        # r = res
        # o = out

        # Db, Da = self.IP_change(x_k, z_k)
        # ai += Da
        # bi += Db

        return res,out,inp

    def step(self, u):
        [res,out,inp],updates = theano.scan(self.scanner,
                sequences=[u],
                outputs_info=[
                    self.res,
                    self.out,
                    None
                ],
                non_sequences=[self.ai,self.bi,self.res,self.out])

        return out,updates
    '''
    def run(self,):

        inp,res,out = self.step(self.u)

        return theano.function(
            inputs=[self.u, self.dt],
            outputs=[out],
            # updates=([
            #     (self.res,res),
            #     (self.out,out),
            # ])
            )

    def train(self,):
        u_target = TT.vector("u_target")
        utm1 = TT.vector("utm1_target")
        y_target = TT.vector("y_target")
        v_target = TT.vector("v_target")

        x_km1 = TT.vector("x_km1")
        z_km1 = (1+ TT.exp( -( (self.ai*x_km1) + self.bi ) ) )**-1

        x_k = TT.concatenate([self.u, self.y, self.v])
        z_k = self.make_z_k(x_k)

        x_ktp1 = self.make_x_kp1(x_k, z_k)
        z_ktp1 = self.make_z_k(x_ktp1)

        output = z_ktp1[-self.out_s:]
        res = x_ktp1[self.in_s:-self.out_s]
        inp = z_ktp1[:self.in_s]


        tdlr =  self.net_lr * (z_km1.norm(2)**2 + utm1.norm(2)**2)**-1 *\
                z_km1.dimshuffle((0,'x'))

        er_sngl_INP = TT.dot(
            tdlr,
            (u_target - inp ).dimshuffle(('x',0))
            )

        er_sngl_OUT = TT.dot(
            tdlr,
            (v_target - output).dimshuffle(('x',0))
            )

        tdlr_inp = 10**(-3)*self.net_lr * (self.u.norm(2)**-2) *\
                self.u.dimshuffle((0,'x'))

        er_sngl_IO = TT.dot(
            tdlr_inp,
            (v_target - output).dimshuffle(('x',0))
            )

        Dw_in = er_sngl_INP
        Dw_out = er_sngl_OUT

        Db, Da = self.IP_change(x_k,z_k)

        return theano.function(
            inputs=[utm1,self.u, self.y, self.v,
                u_target, v_target,
                x_km1,
                self.dt, self.ip_lr,
                self.miou, self.net_lr],
            outputs=[output,res,inp,z_k[self.in_s:-self.out_s], Db, self.ai ],
            updates=([
                    (self.W_res_inp, self.W_res_inp + Dw_in),
                    (self.W_res_out, self.W_res_out + Dw_out),
                    (self.W_inp_out, self.W_inp_out + er_sngl_IO),
                    (self.ai, self.ai + Da),
                    (self.bi, self.bi + Db)
                    ]),
            )
    '''










class NETWRAPPER:
    def __init__(self, net_in, net_res, net_out,
            net_dt, net_ip_lr, net_miou, net_lr):

        self.net_in = net_in
        self.net_res = net_res
        self.net_out = net_out
        self.net_lr = net_lr

        self.net_dt = net_dt
        self.net_ip_lr = net_ip_lr
        self.net_miou = net_miou

        self.net = ESNIP(net_in, net_res, net_out)
        self.runner = self.net.run()
        self.trainer = self.net.train()
        self.ii = np.zeros(net_in)
        self.oo = np.zeros(net_out)
        self.r = np.zeros(net_res)
        self.z = np.random.random(net_res)
        self.alpha = 1.


    def train(self, trX, trY, epochs, plot=True):

        dt = 1
        gains = []
        biases = []


        skip_step = 40
        trX_tp1 = trX[dt::skip_step]
        trY_tp1 = trY[dt::skip_step]
        trX = trX[:-dt:skip_step]
        trY = trY[:-dt:skip_step]

        # self.r.append(np.zeros(self.net_res)+10**-4)
        # self.r.append(np.zeros(self.net_res)+10**-4)
        erIN_old, erOUT_old = 0, 0

        for __ in xrange(epochs):

            self.ii = np.zeros_like(self.ii)
            self.oo = np.zeros_like(self.oo)
            self.r = np.random.random(self.r.shape)#*0.1
            self.z = np.random.random(self.r.shape)
            x_tm1 = np.zeros_like(self.ii)
            mseIN = 0
            mseOUT = 0
            erIN = []
            erOUT = []
            # erIN = 0
            # erOUT = 0
            out = []
            res = []
            inp = []



            try:
                for _, v in enumerate(zip(zip(trX, trY),zip(trX_tp1, trY_tp1))):
                    t,tp1 = v
                    X,Y = t
                    X_tp1, Y_tp1 = tp1
                    outEr = inEr = np.zeros_like(self.r)
                    # import pdb; pdb.set_trace()


                    if np.all(Y.flatten() == 0.):
                        z, x = self.runner(
                            self.ii, self.r, self.oo,
                            self.net_dt,
                            #self.net_ip_lr, self.net_miou
                            )
                        oo = z[-self.net_out:]
                        self.oo = oo
                        self.r = x[self.net_in:-self.net_out]
                        ii = z[:self.net_in]
                        self.ii = ii
                    else:

                        self.oo,self.r,self.ii, self.z, outEr, inEr = self.trainer(
                            # self.ii,self.r, self.oo,#input
                            x_tm1,
                            X.flatten(),self.r, Y.flatten(),#input
                            X_tp1.flatten(),Y_tp1.flatten(),#output
                            # X_tp1.flatten(),Y_tp1.flatten(),#output
                            self.z,#memory
                            self.net_dt, self.net_ip_lr,#hyper
                            self.net_miou, self.net_lr#hyper
                            )

                        x_tm1 = X.flatten()
                    # print "Z ",np.sum(self.z)
                    # print "y_km1 ", np.sum(inEr)

                    mse_out = np.mean((Y_tp1-self.oo)**2)#/self.net_out
                    mse_in = np.mean((X_tp1-self.ii)**2)#/self.net_in

                    out.append(self.oo)
                    res.append(self.r)
                    inp.append(self.ii)

                    # erIN += np.mean(inEr)
                    # erOUT += np.mean(outEr)
                    erIN.append(inEr)
                    erOUT.append(outEr)
                    mseIN += mse_in
                    mseOUT += mse_out

                gains.append(np.mean(erIN))
                biases.append(np.var(erIN))

            except (KeyboardInterrupt, SystemExit):
                    break

            # if np.any([isinstance(x.op, TT.Elemwise) for x in self.trainer.maker.fgraph.toposort()]):
            #     print('Used the cpu')
            # else:
            #     print('Used the gpu')

            # print "Epoch ", __ ,\
            #     ":\t MSE Error in ", mseIN/len(trX) ," ",erIN,\
            #     "\terror out ", mseOUT/len(trY)," ", erOUT,\
            #     "lr ", self.net_lr
            print "Epoch ", __ ,\
                ":\t MSE Error in ", mseIN/len(trX) ," ",\
                "\terror out ", mseOUT/len(trY)," ",\
                "lr ", self.net_lr
                # import pdb; pdb.set_trace()
            if mseOUT <  10**-4:
                break


            print len(out),len(res),len(inp)
            # if plot:
            #     if __ == 0:
            #         plt.ion()
            #         f, axarr = plt.subplots(3)
            #         plt.show()
            #     elif __ >0:
            #         f.canvas.flush_events()
            #
            #         f, axarr = plt.subplots(3)
            #         out_plot = axarr[0].plot(out,"-", linewidth=1.2)[0]
            #         # out_plot = plt.plot(range(len(out)), out,"-", linewidth=1.2)[0]
            #         axarr[0].set_title('Network Output')
            #         # axarr[2].set_ylabel('Values')
            #         res_plot = axarr[1].plot(res,"-")[0]
            #         # plt.figure()
            #         # res_plot = plt.plot(range(len(res)), res,"-")[0]
            #         axarr[1].set_title('Network Reservoir')
            #         # axarr[2].set_ylabel('Values')
            #         inp_plot = axarr[2].plot(inp,"-", linewidth=1.2)[0]
            #         # plt.figure()
            #         # inp_plot = plt.plot(range(len(inp)), inp,"-", linewidth=1.2)[0]
            #         axarr[2].set_title('Network Input')
            #         axarr[2].set_ylabel('Values')
            #
            #         axarr[0].relim()
            #         axarr[1].relim()
            #         axarr[2].relim()
            #
            #         axarr[0].autoscale()
            #         axarr[1].autoscale()
            #         axarr[2].autoscale()
            #
            #
            #         # plt.draw()
            #         plt.plot()
        if plot:
            f, axarr = plt.subplots(3)
            out_plot = axarr[0].plot(out,"-", linewidth=1.2)[0]
            # out_plot = plt.plot(range(len(out)), out,"-", linewidth=1.2)[0]
            axarr[0].set_title('Network Output')
            # axarr[2].set_ylabel('Values')
            res_plot = axarr[1].plot(res,"-")[0]
            # plt.figure()
            # res_plot = plt.plot(range(len(res)), res,"-")[0]
            axarr[1].set_title('Network Reservoir')
            # axarr[2].set_ylabel('Values')
            # inp_plot = axarr[2].plot(inp,"-", linewidth=1.2)[0]
            inp_plot = axarr[2].plot(gains,"-", linewidth=1.2)[0]
            inp_plot = axarr[2].plot(biases,"--", linewidth=1.2)[0]
            axarr[2].set_title('Network Input')
            axarr[2].set_ylabel('Values')
            # plt.draw()


            plt.show()

                    # import pdb; pdb.set_trace()
                # else:
                #     f.canvas.flush_events()
                #     print len(out),len(res),len(inp)
                #     # out_plot.set_data(range(len(out)), out)
                #     out_plot.set_ydata(out)
                #
                #     # res_plot.set_data(range(len(res)), res)
                #     #
                #     # inp_plot.set_data(range(len(inp)), inp)
                #
                #     axarr[0].relim()
                #     axarr[1].relim()
                #     axarr[2].relim()
                #
                #     axarr[0].autoscale()
                #     axarr[1].autoscale()
                #     axarr[2].autoscale()
                #
                #     plt.draw()

    def run(self, rX = None, rY = None, plot= True, stps= 1, tau = 2):

        out = []
        res = []
        inp = []

        data = []
        # r = np.zeros(self.net_res)
        # oo = np.zeros(self.net_out)
        self.ii = np.zeros(self.net_in)
        self.oo = np.zeros(self.net_out)
        self.r = np.zeros(self.net_res)

        for X,Y in zip(rX,rY):


            # self.oo = Y.flatten()

            for _i_ in range(stps):
                if np.all(Y.flatten() == 0.):
                    # self.ii = (1-self.alpha)*self.ii + self.alpha*X.flatten()
                    z, x = self.runner(
                        self.ii, self.r, self.oo,
                        self.net_dt,
                        #self.net_ip_lr, self.net_miou
                        )
                    # oo = np.clip(z[-self.net_out:],-0.1,0.1)
                    oo = z[-self.net_out:]
                    self.oo = oo
                    self.r = x[self.net_in:-self.net_out]
                    ii = z[:self.net_in]
                    self.ii = ii
                    # self.r.append(r)
                else:
                    self.ii = (1-self.alpha)*self.ii + self.alpha*X.flatten()
                    z, x = self.runner(
                        self.ii, self.r, self.oo,
                        self.net_dt,
                        #self.net_ip_lr, self.net_miou
                        )
                    # oo = np.clip(z[-self.net_out:],-0.1,0.1)
                    oo = z[-self.net_out:]
                    self.oo = oo
                    self.r = x[self.net_in:-self.net_out]
                    ii = z[:self.net_in]
                    self.ii = ii
                    # self.r.append(r)

            out.append(oo)
            res.append(z[self.net_in:-self.net_out])
            inp.append(ii)

        MSE = np.mean(np.sum((rY - out)**2,axis=0)/np.array(
            [ 480.,  496.,  702.,  597.,  547.]))

        MSE_in = np.sqrt(np.mean((rX - inp)**2))

        print "MSE out ", MSE, " | MSE in ", MSE_in
        out = np.array(out)
        res = np.array(res)
        inp = np.array(inp)
        # import pdb; pdb.set_trace()

        if plot:
            f, axarr = plt.subplots(3, sharex=False,sharey=False)
            for i in xrange(out.shape[1]):
                axarr[0].plot(out[:,i],"-", linewidth=1.2, label=i)
                axarr[0].plot(rY[:,i],"--", label=i)
            axarr[0].legend()
            # axarr[0].plot(out[:-tau],out[tau:],"-", linewidth=1.2)
            # axarr[0].plot(rY[:-tau],rY[tau:],"--")
            axarr[0].set_title('Network Output')
            # axarr[2].set_ylabel('Values')
            axarr[1].plot(res,"-")
            axarr[1].set_title('Network Reservoir')
            # axarr[2].set_ylabel('Values')
            axarr[2].plot(inp,"-", linewidth=1.2)
            axarr[2].plot(rX,"--")
            # # axarr[2].plot(inp[:-tau],inp[tau:],"-", linewidth=1.2)
            # # axarr[2].plot(rX[:-tau],rX[tau:],"--")
            axarr[2].set_title('Network Input')
            axarr[2].set_ylabel('Values')
            plt.tight_layout()
            plt.show()
