import numpy as np
from numpy import linalg as LA

import theano
import theano.tensor as TT

from ganRNN import GAN
# from netDeCNN import Net as DeCNN
# from netCNN import Net as CNN
# from net3 import Net as Net3
# from net2 import Net
from esn_ip import ESNIP as Reservoir
import matplotlib.pyplot as plt
from midi.utils import midiread, midiwrite

from loadmusic import *
musicPath = "/home/koloumpras/Development/MusicGen/MusicDatasets/"

datasets = [
            "JSB_Chorales.pickle",
            "MuseData.pickle",
            "Nottingham.pickle",
            "Piano_midi.pickle"
            ]


def print_greyscale(pixels, width=28, height=28):
    def get_single_greyscale(pixel):
        val = 232 + round(pixel * 23)
        return '\x1b[48;5;{}m \x1b[0m'.format(int(val))

    for l in range(height):
        line_pixels = pixels[l * width:(l+1) * width]
        print(''.join(get_single_greyscale(p) for p in line_pixels))


if __name__ =="__main__":
    dataset = datasets[2]

    data = loadMusic(musicPath+dataset)
    train = parseToSparse(data["train"])
    valid = parseToSparse(data["valid"])
    test = parseToSparse(data["test"])
    # import pdb; pdb.set_trace()

    in_dim = 87
    out_dim = in_dim
    in_g = 32

    # minibatch size
    print "Compiling ... "

    # generator = DeCNN(in_g,in_dim,2048,64,m)
    # discriminator = CNN(in_dim,1,1024,32,m)
    # discriminator = Net3(in_dim,1,240,120,25,10,m)
    # generator = Net(in_g,in_dim,1024,m)
    # discriminator = Net(in_dim,1,784*2,m)
    generator = Reservoir(in_g,128,out_dim)
    discriminator = Reservoir(in_dim,128,1,output_sparsity=.01)

    gan = GAN(generator,discriminator)
    gan.trainD()
    gan.trainG()
    gan.stepD()
    gan.stepG()
    print " ... done"

    try:
        gan.load(epochs=27)
    except Exception as e:
        print "Could not load file ", e
    else:
        print "Parameters loaded"

    # lol
    inp = train
    # import pdb;pdb.set_trace()
    inp_shape = inp[0].shape[0]
    m = inp_shape


    test_range = 5
    gens = []
    des = []
    for t in xrange(test_range):
        z = np.random.random(
                (m,in_g)
                ).astype(theano.config.floatX)
        tune = gan.stepperG(z)
        piano_roll = tune
        # import pdb; pdb.set_trace()
        piano_roll[tune>tune.mean(axis=0)] = 1.
        piano_roll[tune<=tune.mean(axis=0)] = 0.
        midiwrite("tune_test_",t,".mid", piano_roll, (21,109), 0.3)
        gens.append(tune[-1])
        np.save("tune", tune)
        des.append(gan.stepperD(gan.stepperG(z))[-1])


    print "Generated"
    for g,d  in zip(gens,des):
        print "@@@"
        print_greyscale(g,width=10,height=9)
        print "This comes from the dataset, ",d[0]*100,"%! LOL"
'''
# z = np.random.multivariate_normal(
#                     mean,cov,(m)
#                     ).astype(theano.config.floatX)
'''
